import React from 'react'
import ReactDOM from 'react-dom'

import data from './data.json'

const ProductList = React.createClass({
	render: function() {
		const product = data.fort_knight;
		console.log('PRODUCT', product);
		return (
			<div className='ui items'>
				<Product 
					name={product.name}
					desc={product.desc}
					submitUser={product.submitUser}
					score={product.score}
				/>
			</div>
		)
	},
});

const Product = React.createClass({
	render: function() {
		return (
			<div className='item'>
				<div className='middle aligned content'>
					<div className='description'>
						<p>{this.props.name}</p>
						<p>{this.props.desc}</p>
					</div>
					<div className='extra'>
						<span>Submitted by:</span>
						<p>{this.props.submitUser}</p>
					</div>
				</div>				
			</div>
		)
	},
})

ReactDOM.render(
	<ProductList />,
	document.getElementById('app'),
)